import ConfigParser
import logging
from modules import app
from modules import filereader as freader
from modules.datamodel import albumlist
from logging.handlers import RotatingFileHandler

def init(app):
  config = ConfigParser.ConfigParser()

  try:
    config_location = "modules/etc/config.cfg"
    config.read ( config_location )
    app.config['DEBUG'] = config.get("config","debug")
    app.config['ip_address'] = config.get("config","ip_address")
    app.config['port'] = config.get("config","port")
    app.config['url'] = config.get("config","url")
    app.config['log_file'] = config.get("logging", "name")
    app.config['log_location'] = config.get("logging", "location")
    app.config['log_level'] = config.get("logging", "level")

    print ("**** config loadeda ****")
  except :
    print " Could not read configs from : " , config_location

def logs(app):
  log_pathname = app.config['log_location'] + app.config['log_file']
  file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1024*10,backupCount=1024)
  file_handler.setLevel( app.config['log_level'])
  formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
  file_handler.setFormatter(formatter)
  app.logger.setLevel(app.config['log_level'])
  app.logger.addHandler(file_handler)

if __name__ == "__main__":
  init(app)
  logs(app)
  app.logger.info("Server Started")
  print ("**** READ CSV DATABASE ****")
  ### read database files from CSV
  ## freader.readFile()
  print ("**** FINISHED READING CSV DATABASE ****")
    
  print ("**** START SERVER ****")
  
  ##app.run(host=app.config['ip_address'], port=int(app.config['port'],
  ##debug=True))
  ### user reloader avoid running server twice in debug mode^
  app.run(host='0.0.0.0',debug=True, use_reloader=False)

