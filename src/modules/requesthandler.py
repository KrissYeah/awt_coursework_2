﻿from modules import app
from flask import Flask, render_template, url_for, request, redirect, abort, g, session
from modules.datamodel import albumlist as list
import modules.globalmember as gl
import sqlite3
import os.path
### not working
db_login_location = '/var/users2.db'
### working
base_dir = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(base_dir, "var/users.db")

## should be not here...
app.secret_key = 'uniquesecretkey'


def get_db():
  db = getattr(g, 'db', None)
  if (db is None):
    db = sqlite3.connect(db_path)
    g.db = db
  return db

## debug database
@app.route('/database/')
def database():
  db = get_db()
  print base_dir
  print db_path
  sql = "Select rowid, * FROM users ORDER BY username"
  s = []
  for row in g.db.cursor().execute(sql):
    s.append(row[1])
  return '\t'.join(s)


# main route
@app.route('/')
def index():
  return render_template('login.html')

@app.route('/config/')
def config():
    app.logger.info("config was displayed")
    str = []
    ##str.append ('Debug : ' + str(app.config['DEBUG']))
    str.append ('port : ' + app.config['port'])
    str.append ('url : ' + app.config['url'])
    str.append ('ip_address : ' + app.config['ip_address'])
    return '\t'.join(str)


@app.route('/findusers/', methods=['POST', 'GET'])
def finduser():
  if request.method == 'POST':
    req = request.form['userrequest']
    db = get_db()
    sql = "SELECT * FROM users WHERE username LIKE '%{term}%'".format(term=req)
    rows = g.db.cursor().execute(sql)
    output = []

    app.logger.info("user " + session['username'] + " requested " + req)
    for r in rows:
      print r
      print req
      if (req.lower() in r[0].lower()):
        output.append(r[0])
    return render_template('searchview.html', userlist=output, username=session['username'])


@app.route('/logged/')
def privatearea():
  try:
    if(session['username']):
      return render_template('personal.html', username=session['username'])
  except KeyError:
    pass
  return redirect(url_for('index'))


@app.route('/login/', methods=['POST','GET'])
def login():
  if request.method == 'POST':
    username = request.form['user']
    password = request.form['pass']
    db = get_db()
    sql = "SELECT * FROM users WHERE username=?"
    row = g.db.cursor().execute(sql,[(username)])
    
    for r in row:
      if (r[1] == password):
        session['username'] = username
        app.logger.info("user " + username + " logged in")
        return privatearea()
      else: 
        return "wrong password"
  return "Username not found"

@app.route('/logout/')
def logout():
  app.logger.info("user " + session['username'] + "logged out")
  session.pop('username', None)
  return render_template('login.html')
    

@app.route('/createuser/', methods=['POST','GET'])
def createuser():
  if request.method == 'POST':
    username = request.form['us']
    password = request.form['pw']
    db = get_db()
    g.db.cursor().execute('INSERT into users values (?, ?)', [username,password])
    g.db.commit()
    app.logger.info("new user " + username + " was created")
    return render_template('login.html')
  return

#### ERROR HANDLER ####
@app.errorhandler(404)
def page_not_found(error):
  return render_template('errorpage.html'), 404
