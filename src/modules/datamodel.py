
albumlist = []

class Album():
  def __init__(self, pos, artist, name, year, final_score, usa, eng, eur, raw):
    self.pos = pos
    self.artist = artist
    self.name = name
    self.year = year
    self.final_score = final_score
    self.usa = usa
    self.eng = eng
    self.eur = eur
    self.raw = raw

  def __getitem__(self, index):
    return index;

  def toString(self):
    return "{0} {1} {2} {3} {4} {5} {6} {7} {8}".format(self.pos,self.artist,self.name,self.year,self.final_score, self.usa, self.eng,self.eur, self.raw)
