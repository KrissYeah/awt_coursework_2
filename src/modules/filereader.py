from modules import app
from modules.datamodel import Album, albumlist
import modules.globalmember as gl
import csv

### readfile function would be better with parameter as string 
### but not importand here...
def readFile():
  data_file = open('modules/static/csv/data.csv')
  csv_file = csv.reader(data_file)
  
  ### iterate thre csv file and add to list
  for row in csv_file:
    albumlist.append(Album(int(row[0]), row[1], row[2], int(row[3]), row[4], row[5], 
    row[6], row[7], row[8]))
  
  ### print length of list in console
  if (gl.DEBUGMODE_PRINT):
    print ("** Albumlist Size: {0} **".format(len(albumlist)))

